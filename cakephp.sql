-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2015 at 02:29 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cakephp`
--
CREATE DATABASE IF NOT EXISTS `cakephp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cakephp`;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `code` varchar(25) NOT NULL,
  `flagged` int(1) NOT NULL,
  `flagged_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `image`, `description`, `created`, `modified`, `code`, `flagged`, `flagged_id`) VALUES
(12, 3, '1234', 'http://placehold.it/300x300', '<p><strong>21</strong>4</p>', '2015-02-10 14:41:04', '2015-02-11 14:22:44', 'dvtkXr79hRfJ5VhYzUhrveMvF', 1, 0),
(13, 3, 'affff', 'ffff', '<p>ffff</p>', '2015-02-10 14:41:25', '2015-02-11 14:17:35', 'fasfasfasfa', 0, 0),
(18, 3, 'New Post', 'http://placehold.it/300x300', '<p><strong style="font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;">&nbsp;is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. <em>Het heeft niet alleen vijf</em> eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren ''60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals <strong>Aldus PageMaker die versies van Lorem Ipsum bevatten.</strong></span></p>', '2015-02-10 15:06:01', '2015-02-11 14:20:13', '', 1, 0),
(22, 2, 'myenwpost', 'http://placehold.it/300x300', '<p>testt</p>', '2015-02-11 14:23:31', '2015-02-11 14:25:43', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `full_name`, `created`, `modified`) VALUES
(1, 'test', '41265df7c478c0900bc3bbeed8587676870a0159', 1, 'test333', '2015-02-10 10:57:42', '2015-02-11 11:34:55'),
(2, 'reguse', 'aaba36e18405ccf17772def354f29b2e80b52265', 1, 'reguse', '2015-02-10 11:22:33', '2015-02-10 11:22:33'),
(3, 'admin', 'c68573d19f4b3da72e30761064da40f770571b96', 2, 'admin', '2015-02-10 11:26:49', '2015-02-10 11:26:49'),
(4, 'test321', '57da87598f151ce30b4f4a29797524149559491d', 1, 'test321', '2015-02-10 15:24:42', '2015-02-10 15:24:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
