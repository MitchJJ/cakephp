<?php

	class PostsController extends AppController{

		public $components = array('Session');

		public function beforeFilter(){
			$this->Auth->allow('index');
		}

		public function index(){
			$data = $this->Post->find('all');
			$this->set('posts', $data);
		}

		public function add(){
			if($this->request->is('post')){
				$this->Post->create();
				$this->request->data['Post']['user_id'] = AuthComponent::user('id');
				if($this->Post->save($this->request->data)){
					$this->Session->setFlash('Post created!');
					$this->redirect('index');
				}
			}
		}

		public function view($id = NULL){
			if($id == NULL)
			throw new Exception('No ID found!');
			/*
			Warning (2): Illegal string offset 'flagged' [APP\Controller\PostsController.php, line 30]
			Ik krijg hier steeds deze foutmelding, de if statement wordt wel gewoon uitgevoerd zoals ik wil. Ik krijg deze warning alleen niet weg.
			*/
			$data = $this->Post->findById($id);
			if($id["flagged"] == "1" && AuthComponent::user('role') != 2){
				$this->Session->setFlash('You are not authorized to view this post!');
				$this->redirect('index');
			}
			$this->set('post', $data);
		}

		public function edit($id){

			if(AuthComponent::user('role') == 1){
				$this->redirect('index');
			}

			if(isset($javascript)){
				echo $javascript->link('jquery.js'); 
				echo $javascript->link('tiny_mce.js'); 
			}

			$data = $this->Post->findById($id);

			if($this->request->is(array('post', 'put'))){
				$this->Post->id = $id;
				if($this->Post->save($this->request->data)){
					$this->Session->setFlash('Edit successful!');
					$this->redirect('index');
				}
			}
			$this->request->data = $data;
		}

		public function delete($id){

			if(AuthComponent::user('role') == 1)
			$this->redirect('index');

			$this->Post->id = $id;
			if($this->request->is(array('post', 'put', $this->Post->delete()))){
				$this->Session->setFlash('The page has been deleted!');
				$this->redirect('index');
			}
		}

		public function flagPost($id){
			$data = array('Post' => array('id' => $id, 'flagged' => '1'));
			$this->request->data['Post']['flagged_id'] = AuthComponent::user('id');

			$this->Post->save($data);
			$this->Session->setFlash('The post has been flagged!');
			$this->redirect('index');
		}

		public function unflagPost($id){
			$data = array('Post' => array('id' => $id, 'flagged' => '0'));

			$this->Post->save($data);
			$this->Session->setFlash('The post has been unflagged!');
			$this->redirect('index');
		}
	}