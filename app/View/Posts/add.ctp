<script type="text/javascript" src="../app/webroot/js/tinymce.min.js"></script>
<h1>Add Page</h1>
<?php
	echo $this->Form->create('Post');
	// echo $this->Form->input('code', array('id' => 'code','placeholder'=>'Please enter your URL'));
	// echo "<input type='button' id='button' value='Generate URL' onclick='makeid()'>";
	echo $this->Form->input('title', array('placeholder'=>'Please enter a title'));
	echo $this->Form->input('image', array('placeholder'=>'http://placehold.it/300x300'));
	echo $this->Form->input('description', array('placeholder'=>'Please enter your description'));
	echo $this->Form->end('Save Post');
?>
<script type="text/javascript"> 
    tinyMCE.init({ 
        theme : "modern", 
        mode : "textareas", 
        convert_urls : false 
    }); 
    var code = document.getElementById('code');

    function makeid(){
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 25; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    code.value = text;
	}
</script> 