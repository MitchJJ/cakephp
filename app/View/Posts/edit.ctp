<script type="text/javascript" src="../../app/webroot/js/tinymce.min.js"></script>
<h1>Edit Page</h1>
<?php 
	if(AuthComponent::user()){
		echo $this->HTML->link('Logout', array('controller' => 'users', 'action' => 'logout'));
	}
	else{
		echo $this->HTML->link('Login', array('controller' => 'users', 'action' => 'login'));
	}
?>
<?php
	echo $this->Form->create('Post');
	// echo $this->Form->input('code', array('id' => 'code','placeholder'=>'Please enter your URL'));
	// echo "<input type='button' id='button' value='Generate URL' onclick='makeid()'>";
	echo $this->Form->input('title');
	echo $this->Form->input('image');
	?>
	<?php
	echo $this->Form->input('description');
	echo $this->Form->end('Edit Post');
	?>
<script type="text/javascript"> 
    tinyMCE.init({ 
        theme : "modern", 
        mode : "textareas", 
        convert_urls : false 
    }); 
    var code = document.getElementById('code');

    function makeid(){
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 25; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    code.value = text;
	}
</script> 