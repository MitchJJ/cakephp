<h1>Posts</h1>
<?php echo $this->HTML->link('Create new post', array('controller' => 'posts', 'action' => 'add')); ?>
<br>
<?php 
	if(AuthComponent::user()){
		echo $this->HTML->link('Logout', array('controller' => 'users', 'action' => 'logout'));
	}
	else{
		echo $this->HTML->link('Login', array('controller' => 'users', 'action' => 'login')); echo "||"; echo $this->HTML->link('Register', array('controller' => 'users', 'action' => 'add'));
	}
	if(AuthComponent::user('role') == 2){
		echo '<br>';
		echo $this->HTML->link('Control Panel', array('controller' => 'users', 'action' => 'index'));
	}
?>
<table>
<tr>
	<th>Title</th>
	<th>User</th>
	
	<th>Created</th>
	<th>Modified</th>
	<?php if(AuthComponent::user('role') == 2) : ?>
	<th>Edit</th>
	<th>Delete</th>
	<?php endif; ?>
</tr>
<?php foreach($posts as $post) : ?>
<tr>

<?php if(AuthComponent::user('role') == 2) : ?>
	<td><?php echo $this->HTML->link($post['Post']['title'], array('controller' => 'Posts', 'action'=> 'view', $post['Post']['id'])); ?></td>
	<td><?php echo $post['User']['username']; ?></td>
	
	<td><?php echo $post['Post']['created']; ?></td>
	<td><?php echo $post['Post']['modified']; ?></td>
	<?php if(AuthComponent::user('role') == 2) : ?>
	<td><?php echo $this->HTML->link('Edit', array('controller' => 'Posts', 'action'=> 'edit', $post['Post']['id'])); ?></td>
	<td><?php echo $this->Form->postLink('Delete', array('controller' => 'Posts', 'action'=> 'delete', $post['Post']['id']), array('confirm' => 'Are you sure you want to delete this Post?')); ?></td>
	<?php endif; ?>
</tr>
<?php endif; ?>
<?php if(AuthComponent::user('role') == 1 || !AuthComponent::user()) : ?>
	<td><?php echo $this->HTML->link($post['Post']['title'], array('controller' => 'Posts', 'action'=> 'view', $post['Post']['id'])); ?></td>
	<td><?php echo $post['User']['username']; ?></td>
	
	<td><?php echo $post['Post']['created']; ?></td>
	<td><?php echo $post['Post']['modified']; ?></td>
	<?php if(AuthComponent::user('role') == 2) : ?>
	<td><?php echo $this->HTML->link('Edit', array('controller' => 'Posts', 'action'=> 'edit', $post['Post']['id'])); ?></td>
	<td><?php echo $this->Form->postLink('Delete', array('controller' => 'Posts', 'action'=> 'delete', $post['Post']['id']), array('confirm' => 'Are you sure you want to delete this Post?')); ?></td>
	<?php endif; ?>
</tr>
<?php endif; ?>
<?php endforeach; ?>
<?php unset($post); ?>
</table>
<!--  -->
<?php if(AuthComponent::user('role') == 2) : ?>
<h2>Flagged Posts</h2>

<table>
<tr>
	<th>Title</th>
	<th>User</th>
	<th>Created</th>
	<th>Modified</th>
	<th>Flagged by</th>
	<?php if(AuthComponent::user('role') == 2) : ?>
	<th>Unflag</th>
	<th>Delete</th>
	<?php endif; ?>
</tr>
<?php endif; ?>
<?php foreach($posts as $post) : ?>
<tr>
<?php if(AuthComponent::user('role') == 2) : ?>
<?php if($post['Post']['flagged'] == "1") : ?>
	<td><?php echo $this->HTML->link($post['Post']['title'], array('controller' => 'Posts', 'action'=> 'view', $post['Post']['id'])); ?></td>
	<td><?php echo $post['User']['username']; ?></td>
	
	<td><?php echo $post['Post']['created']; ?></td>
	<td><?php echo $post['Post']['modified']; ?></td>
	<td><?php echo $post['User']['username']; ?></td>
	<td><?php echo $this->Form->postLink('Unflag', array('controller' => 'Posts', 'action'=> 'unflagPost', $post['Post']['id']), array('confirm' => 'Are you sure you want to unflag this post?')); ?></td>
	<td><?php echo $this->Form->postLink('Delete', array('controller' => 'Posts', 'action'=> 'delete', $post['Post']['id']), array('confirm' => 'Are you sure you want to delete this post?')); ?></td>
	<?php endif; ?>
</tr>
<?php endif; ?>
<?php endforeach; ?>
<?php unset($post); ?>
</table>