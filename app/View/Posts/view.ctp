<?php 
	if(AuthComponent::user()){
		echo $this->HTML->link('Logout', array('controller' => 'users', 'action' => 'logout'));
	}
	else{
		echo $this->HTML->link('Login', array('controller' => 'users', 'action' => 'login'));
	}
?>
<br><br>
<div id="postContainer">
<?php 
	echo "<h1 class='postTitle'>".$post['Post']['title']."</h1>"; 
	echo "<img src=".$post['Post']['image']." class='postImage'>"; 
	echo "<div class='postDescription'>".$post['Post']['description']."</div>"; 
?>
</div>
<?php echo $this->Form->postLink('Flag Post', array('controller' => 'Posts', 'action'=> 'flagPost', $post['Post']['id']), array('confirm' => 'Are you sure you want to flag this Post?')); ?>

<style>
	#postContainer{
		height: 400px;
	}
	.postTitle{
		font-size:25px;
		float:left;
		height: 25px;
		text-transform: uppercase;
	}
	.postImage{
		position: absolute;
		left: 20px;
		margin-top:35px;
		max-width: 300px;
		max-height: 300px;
	}
	.postDescription{
		position: absolute;
		width: calc(100% - 350px);
		padding-top:10px;
		padding-left:10px;
		height: 300px;
		left:350px;
		margin-top:35px;
		background-color: #f5f5f5;
	}
</style>